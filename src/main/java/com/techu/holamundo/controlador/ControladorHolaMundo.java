package com.techu.holamundo.controlador;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControladorHolaMundo {
    @RequestMapping("/saludo")
    public String saludar() {
        return "Hola Tech U!";
    }
}
